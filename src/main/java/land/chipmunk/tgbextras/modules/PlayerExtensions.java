package land.chipmunk.tgbextras.modules;

import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerExtensions {
  ServerPlayerEntity lastTeleportRequestor ();
  ServerPlayerEntity lastTeleportRequestor (ServerPlayerEntity lastTeleportRequestor);
  Boolean AFK ();
  void AFK (ServerPlayerEntity player);
}
