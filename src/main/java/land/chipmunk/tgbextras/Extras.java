package land.chipmunk.tgbextras;

import land.chipmunk.tgbextras.items.CocaineItem;
import land.chipmunk.tgbextras.items.FermentedLeavesItem;
import land.chipmunk.tgbextras.items.RawLeavesItem;
import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.command.CommandManager;
import land.chipmunk.tgbextras.commands.*;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Extras implements ModInitializer {
  public static final Logger LOGGER = LoggerFactory.getLogger("data/tgb-extras");
  public static final Item COCAINE = new CocaineItem(new FabricItemSettings().food(new FoodComponent.Builder().hunger(5).saturationModifier(.5f).statusEffect(
          new StatusEffectInstance(StatusEffects.SPEED, 2400, 2), 1f).statusEffect(
                  new StatusEffectInstance(StatusEffects.HASTE, 2400, 2), 1f).statusEffect(
                          new StatusEffectInstance(StatusEffects.STRENGTH, 2400, 1),1f).build()));
  public static final Item FERMENTED_LEAVES = new FermentedLeavesItem(new FabricItemSettings());
  public static final Item RAW_LEAVES = new RawLeavesItem(new FabricItemSettings());
  @Override
  public void onInitialize () {
    LOGGER.info("Extras initialized!");
    CommandRegistrationCallback.EVENT.register(Extras::registerCommands);
    registerItems();
  }
  public static void registerItems (){
    ItemGroupEvents.modifyEntriesEvent(ItemGroups.FOOD_AND_DRINK).register(content -> {
      content.add(COCAINE);
    });
    ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(content -> {
      content.add(FERMENTED_LEAVES);
      content.add(RAW_LEAVES);
    });
    Registry.register(Registries.ITEM, new Identifier("tgb-extras", "cocaine"), COCAINE);
    Registry.register(Registries.ITEM, new Identifier("tgb-extras", "raw_leaves"), RAW_LEAVES);
    Registry.register(Registries.ITEM, new Identifier("tgb-extras", "fermented_leaves"), FERMENTED_LEAVES);
  }
  public static void registerCommands (CommandDispatcher<ServerCommandSource> dispatcher, CommandRegistryAccess commandRegistryAccess, CommandManager.RegistrationEnvironment environment) {
    ClearChatCommand.register(dispatcher);
    PingCommand.register(dispatcher);
    TGBCommand.register(dispatcher);
    SpawnCoordinatesCommand.register(dispatcher);
    TPAskCommand.register(dispatcher);
    TPAcceptCommand.register(dispatcher);
    TPDenyCommand.register(dispatcher);
    AFKCommand.register(dispatcher);
  }
}