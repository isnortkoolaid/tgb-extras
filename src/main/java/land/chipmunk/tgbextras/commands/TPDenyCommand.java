package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.server.network.ServerPlayerEntity;
import land.chipmunk.tgbextras.modules.PlayerExtensions;
import java.util.Collections;

public class TPDenyCommand {
  private static SimpleCommandExceptionType NO_PENDING_TELEPORT_REQUEST_EXCEPTION = new SimpleCommandExceptionType(Text.literal("There is no pending teleport request"));

  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher.register(
      literal("tpdeny")
        .executes(TPDenyCommand::tpDenyCommand)
    );
  }

  public static int tpDenyCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();

    final PlayerExtensions playerEx = (PlayerExtensions) (Object) player;
    final ServerPlayerEntity requestor = playerEx.lastTeleportRequestor();

    if (requestor == null) {
      throw NO_PENDING_TELEPORT_REQUEST_EXCEPTION.create();
    }

    playerEx.lastTeleportRequestor(null);

    source.sendFeedback(() -> Text.literal("Rejected teleport request from ").append(requestor.getDisplayName()), true);

    return Command.SINGLE_SUCCESS;
  }
}
