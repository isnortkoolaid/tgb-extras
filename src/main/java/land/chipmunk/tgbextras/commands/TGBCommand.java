package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.argument.EntityArgumentType.players;
import static net.minecraft.command.argument.EntityArgumentType.getPlayers;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.server.network.ServerPlayerEntity;
import java.util.Collection;
import java.util.List;

public class TGBCommand {
  private static String[][] WORDS = {
    {"The", "Traumatic", "Time (to)"},
    {"Good", "Gyat", "Gigantic", "Grind", "Griddy"},
    {"Burger", "Boys", "Butts", "Bentonite"}
  };

  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher.register(
      literal("tgb")
        .executes(TGBCommand::tgbCommand)
    );
  }

  public static int tgbCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    source.sendFeedback(() -> Text.literal(generate()), false);

    return Command.SINGLE_SUCCESS;
  }

  private static String generate () {
    final String[] words = new String[WORDS.length];

    for (int i = 0; i < words.length; i++) {
      final String[] possibleWords = WORDS[i];
      final String word = possibleWords[(int)(Math.random() * possibleWords.length)];
      words[i] = word;
    }

    return String.join(" ", words);
  }
}
