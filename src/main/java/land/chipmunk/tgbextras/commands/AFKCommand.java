package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import land.chipmunk.tgbextras.modules.PlayerExtensions;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import static net.minecraft.server.command.CommandManager.literal;

public class AFKCommand {
    public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(
                literal("afk")
                        .executes(AFKCommand::AFKCommand)
        );
    }
    public static int AFKCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        final ServerCommandSource source = context.getSource();
        final ServerPlayerEntity player = source.getPlayerOrThrow();
        PlayerExtensions target =  (PlayerExtensions) player;
        target.AFK(player);
        if(target.AFK()) {
            player.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 2147483647, 4, true, false));
            player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 2147483647, 99, true, false));
            player.addStatusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS, 2147483647, 99, true, false));
            player.addStatusEffect(new StatusEffectInstance(StatusEffects.WEAKNESS, 2147483647, 99, true, false));
            player.addStatusEffect(new StatusEffectInstance(StatusEffects.MINING_FATIGUE, 2147483647, 99, true, false));
        } else {
            player.clearStatusEffects();
        }
        return Command.SINGLE_SUCCESS;
    }
}
