package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import static net.minecraft.server.command.CommandManager.literal;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;
import land.chipmunk.tgbextras.modules.PlayerExtensions;
import java.util.Collections;

public class TPAcceptCommand {
  private static SimpleCommandExceptionType NO_PENDING_TELEPORT_REQUEST_EXCEPTION = new SimpleCommandExceptionType(Text.literal("There is no pending teleport request"));

  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher.register(
      literal("tpaccept")
        .executes(TPAcceptCommand::tpAcceptCommand)
    );
  }

  public static int tpAcceptCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();

    final PlayerExtensions playerEx = (PlayerExtensions) (Object) player;
    final ServerPlayerEntity requestor = playerEx.lastTeleportRequestor();

    if (requestor == null) {
      throw NO_PENDING_TELEPORT_REQUEST_EXCEPTION.create();
    }

    final Vec3d pos = player.getPos();

    requestor.teleport(
      (ServerWorld)player.getWorld(),
      pos.getX(),
      pos.getY(),
      pos.getZ(),
      Collections.emptySet(),
      player.getYaw(),
      player.getPitch()
    );
    playerEx.lastTeleportRequestor(null);

    source.sendFeedback(() -> Text.literal("Accepted teleport request from ").append(requestor.getDisplayName()), true);

    return Command.SINGLE_SUCCESS;
  }
}
