package land.chipmunk.tgbextras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import net.minecraft.server.network.ServerPlayerEntity;
import land.chipmunk.tgbextras.modules.PlayerExtensions;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin implements PlayerExtensions {
  private ServerPlayerEntity lastTeleportRequestor = null;
  private Boolean AFK = false;
  public Boolean AFK() {
    return AFK;
  }
  public void AFK(ServerPlayerEntity player) {this.AFK = !AFK;}
  public ServerPlayerEntity lastTeleportRequestor () {
    return lastTeleportRequestor;
  }

  public ServerPlayerEntity lastTeleportRequestor (ServerPlayerEntity lastTeleportRequestor) {
    return (this.lastTeleportRequestor = lastTeleportRequestor);
  }
}